<?php
if ( $page_for_posts_id = get_option( 'page_for_posts' ) ) {
    $posts_page_url = get_permalink($page_for_posts_id);
}
?>
<?php get_header(); ?>
<div class="row">
    <?php dynamic_sidebar('page-top-widget-area'); ?>
    <?php if (function_exists('get_field') && $pageTop = get_field( 'acf_page_top' )) { echo $pageTop; } ?>
</div>
<div class="row">
    <div class="col-md-3 hidden-sm hidden-xs">
        <?php get_sidebar(); ?>
    </div>
    <div class="col-md-6">
        <main role="main">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php dynamic_sidebar('page-main-widget-area'); ?>
                    <h1><?php the_title(); ?></h1>
                    <?php the_content(); // Dynamic Content ?>
                    <?php edit_post_link(); // Always handy to have Edit Post Links available ?>
                </article>
            <?php endwhile; ?>
            <?php else: ?>
                <article>
                    <h1><?php _e( 'Sorry, nothing to display.', 'diclectin' ); ?></h1>
                </article>
            <?php endif; ?>
        </main>
    </div>
    <div class="col-md-3">
        <?php get_sidebar( 'two' ); ?>
    </div>
</div>
<?php get_footer(); ?>
