<?php get_header(); ?>
<div class="row">
<?php dynamic_sidebar('page-top-widget-area'); ?>
<?php
    $queried_object = get_queried_object();
    $taxonomy = $queried_object->taxonomy;
    $term_id = $queried_object->term_id;

    if (function_exists('get_field') && $queried_object && $page_top = get_field('acf_page_top', $queried_object)) {
        echo $page_top;
    }
?>
</div>
<div class="row">
    <div class="col-md-3 hidden-sm hidden-xs">
        <?php get_sidebar(); ?>
    </div>
    <div class="col-md-6">
        <main role="main">
            <?php dynamic_sidebar('page-main-widget-area'); ?>
            <h1><?php single_cat_title(); ?></h1>
            <?php get_template_part('loop'); ?>
            <?php get_template_part('pagination'); ?>
        </main>
    </div>
    <div class="col-md-3">
        <?php get_sidebar('two'); ?>
    </div>
</div>
<?php get_footer(); ?>
