<?php
/*
Template Name: Affiliate Page
*/

?>
<?php get_header(); ?>


[affiliates_is_not_affiliate]

Please log in to access the affiliate area.

[affiliates_login_redirect]

If you are not an affiliate, you can join the affiliate program here:

[affiliates_registration terms_post_id="256" /]
[/affiliates_is_not_affiliate]

[affiliates_is_affiliate]

Welcome to your affiliate area. Here you can find information about your affiliate link and earnings.
<h3>Affiliate Information</h3>
[affiliates_fields]
<h3>Affiliate link</h3>
Your affiliate URL:
<code>[affiliates_url]</code>

Use this code to embed your affiliate link:
<code>&lt;a href="[affiliates_url]"&gt;Affiliate link&lt;/a&gt;</code>

Tip: You should change the text <em>Affiliate link</em> to something more attractive.
<h3>Performance</h3>
<h4>Total Earnings</h4>
<h5>Commissions pending payment</h5>
[affiliates_referrals show="total" status="accepted"]
<h5>Commissions paid</h5>
[affiliates_referrals show="total" status="closed"]
<h4>Number of sales referred</h4>
<ul>
	<li>Accepted referrals pending payment: [affiliates_referrals status="accepted"]</li>
	<li>Referrals paid: [affiliates_referrals status="closed"]</li>
</ul>
<h4>Monthly Earnings</h4>
[affiliates_earnings]
[affiliates_logout]
[/affiliates_is_affiliate]

<?php get_footer(); ?>