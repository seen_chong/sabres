<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(array('clearfix')); ?>>
		<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_post_thumbnail( array(120, 120), array( 'class' => 'alignleft' )); // Declare pixel size you need inside the array ?>
			</a>
		<?php endif; ?>
		<h2>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" style="font-size: 16px; text-decoration: none; font-weight: bold;"><?php the_title(); ?></a>
		</h2>
		<span class="comments"><?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'diclectin' ), __( '1 Comment', 'diclectin' ), __( '% Comments', 'diclectin' )); ?></span>
		<?php sabres_wp_excerpt(50); // Build your custom callback length in functions.php ?>
		<?php edit_post_link(); ?>
	</article>
<?php endwhile; ?>

<?php else: ?>
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'diclectin' ); ?></h2>
	</article>
<?php endif; ?>
