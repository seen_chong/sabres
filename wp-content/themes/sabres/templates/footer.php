<?php
    if (function_exists('ot_get_option')) {
        $footer_logo = ot_get_option('logo_footer', '');
        $copyright = ot_get_option('copyright', '');
        $contact_email_address = ot_get_option('contact_email_address', '');
        $contact_mobile = ot_get_option('contact_mobile', '');
        $contact_address = ot_get_option('contact_address', '');
    }
?>
<footer>
    <?php if ( isset( $footer_logo ) && $footer_logo != '' ) : ?>
        <a id="footer-logo" class="footer-logo" href="<?php echo get_home_url(); ?>">
            <img src="<?php echo $footer_logo; ?>" alt="" title="" />
        </a>
    <?php endif; ?>
    <div class="container hidden-xs">
        <div class="row">
            <div class="footer-1 col-md-3 col-sm-4 col-xs-12">
                <nav class="footer-menu" role="navigation">
                    <h4>Menu</h4>
                    <?php wp_nav_menu( array('theme_location' => 'footer-menu' )); ?>
                </nav>
                <?php dynamic_sidebar('footer-widget-area-1'); ?>
            </div>
            <div class="footer-2 col-md-3 col-sm-4 col-xs-12">
                <h4>Address</h4>
                <ul>
                    <?php if ( isset( $contact_email_address ) && $contact_email_address != '') : ?>
                        <li class="icon-sm-plane icon">
                            <a href="mailto:<?php echo $contact_email_address; ?>"><?php echo $contact_email_address; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if ( isset( $contact_mobile ) && $contact_mobile != '') : ?>
                        <li class="icon-sm-phone icon">
                            <a href="tel:<?php echo $contact_mobile; ?>"><?php echo $contact_mobile; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if ( isset( $contact_address )&& $contact_address != '') : ?>
                        <li class="icon-sm-compass icon">
                            <p><?php echo $contact_address; ?></p>
                        </li>
                    <?php endif; ?>
                </ul>
                <?php dynamic_sidebar('footer-widget-area-2'); ?>
            </div>
            <div class="footer-3 col-md-4 col-md-offset-2 col-sm-4 col-xs-12">
                <?php dynamic_sidebar('footer-widget-area-3'); ?>
            </div>
        </div>
    </div>
    <?php if ( isset( $copyright ) && $copyright != '') : ?>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="column-content">
                        <?php echo $copyright; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
</footer>