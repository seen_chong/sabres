<?php
    if (function_exists('ot_get_option')) {
        $logo = ot_get_option('logo', '');
    }
?>
<header>
    <div class="container">
        <div class="row hidden-xs">
            <div class="header-1 col-md-6 col-sm-6 hidden-xs">
                <?php dynamic_sidebar('header-widget-area-1'); ?>
            </div>
            <div class="header-2 col-md-6 col-sm-6">
                <?php dynamic_sidebar('header-widget-area-2'); ?>
            </div>
        </div>
        <div class="row hidden-xs">
            <nav class="header-menu" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand logo" href="/">
                            <?php if ( isset($logo) && $logo != '' ) : ?>
                                    <img src="<?php echo $logo; ?>" alt="" title="" />
                            <?php endif; ?>
                        </a>
                    </div>
                    <?php
                    wp_nav_menu( array(
                            'menu'              => 'primary',
                            'theme_location'    => 'header-menu',
                            'container'         => 'div',
                            'container_class'   => 'collapse navbar-collapse',
                            'menu_class'        => 'nav navbar-nav',
                            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                            'walker'            => new wp_bootstrap_navwalker())
                    );
                    ?>
            </nav>
        </div>
    </div>
    <nav class="mobile-menu visible-xs navbar-mobile" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobileMenu" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo" href="/">
                    <?php if ( $logo ) : ?>
                        <img src="<?php echo $logo; ?>" alt="" title="" />
                    <?php endif; ?>
                </a>
            </div>

            <div id="mobileMenu" class="collapse navbar-collapse">
                <div class="container-fluid">
                    <?php wp_nav_menu( array(
                            'menu'              => 'mobile',
                            'theme_location'    => 'mobile-menu',
                            'depth'             => 2,
                            'container'         => 'div',
                            'container_class'   => 'collapse navbar-collapse',
                            'menu_class'        => 'nav navbar-nav',
                            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                            'walker'            => new wp_bootstrap_navwalker())
                    ); ?>
                </div>
            </div>
        </div>
    </nav>
</header>
