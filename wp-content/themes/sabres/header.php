<?php
    $direction = 'ltr';
    if (function_exists('wpml_get_language_information') && get_the_ID()) {
        if ($lang_info = wpml_get_language_information(get_the_ID())) {
            if (isset($lang_info["text_direction"])) {
                $direction = $lang_info["text_direction"] == 1 ? "rtl" : "ltr";
            }
        }
    }
?><!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title><?php bloginfo('name'); ?></title>

        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <link href="" rel="apple-touch-icon-precomposed">

        <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet" media="all" />
        <link href="<?php echo get_template_directory_uri(); ?>/css/fonts/fonts.css" rel="stylesheet" media="all" />
        <?php if ( $direction == 'rtl' ) {  ?>
            <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-rtl.min.css" rel="stylesheet" media="all" />
        <?php } ?>

        <?php wp_head(); ?>

        <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>

        <?php if ( $direction == 'rtl' ) {  ?>
            <link href="<?php echo get_template_directory_uri(); ?>/css/rtl.css" rel="stylesheet" media="all" />
        <?php } ?>


        <style type="text/css">
            body {
                font-family: 'Roboto';
            }
        </style>
	</head>
	<body <?php body_class(); ?>>
        <div id="wrapper" class="wrapper">
            <?php get_template_part('templates/header'); ?>
            <div class="row">
                <?php dynamic_sidebar('top-widget-area'); ?>
            </div>