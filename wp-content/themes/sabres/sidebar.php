<!-- sidebar -->
<aside class="sidebar sidebar-1 hidden-sm hidden-xs" role="complementary">
    <?php if ( function_exists('get_field') && $side_menu = get_field( 'acf_side_menu' ) ) : ?>
        <?php echo $side_menu; ?>
    <?php endif; ?>

	<?php // get_template_part('searchform'); ?>

	<div class="sidebar-widget">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-widget-area-1') ) ?>
	</div>

</aside>
<!-- /sidebar -->
