<?php get_header(); ?>
<div class="row">
    <?php dynamic_sidebar('page-top-widget-area'); ?>
    <?php if (function_exists('get_field') && $pageTop = get_field( 'acf_page_top' )) { echo $pageTop; } ?>
</div>
<div class="row">
    <div class="col-md-3 hidden-sm hidden-xs">
        <?php get_sidebar(); ?>
    </div>
    <div class="col-md-6">
        <main role="main">
            <?php dynamic_sidebar('page-main-widget-area'); ?>
            <h1><?php _e( 'Latest Posts', 'diclectin' ); ?></h1>
            <?php get_template_part('loop'); ?>
            <?php get_template_part('pagination'); ?>
        </main>
    </div>
    <div class="col-md-3">
        <?php get_sidebar( 'two' ); ?>
    </div>
</div>
<?php get_footer(); ?>
