<?php get_header(); ?>
<div class="row">
    <?php dynamic_sidebar('page-top-widget-area'); ?>
    <?php if ($pageTop = get_field( 'acf_page_top' )) { echo $pageTop; } ?>
</div>
<div class="row">
    <div class="col-md-3 hidden-sm hidden-xs">
        <?php get_sidebar(); ?>
    </div>
    <div class="col-md-6">
        <main role="main">
            <?php dynamic_sidebar('page-main-widget-area'); ?>
            <h1><?php the_title(); ?></h1>
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <!-- article -->
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php the_content(); ?>
                    <?php comments_template( '', true ); // Remove if you don't want comments ?>
                    <br class="clear">
                    <?php edit_post_link(); ?>
                </article>
            <?php endwhile; ?>
            <?php else: ?>
                <article>
                    <h2><?php _e( 'Sorry, nothing to display.', 'diclectin' ); ?></h2>
                </article>
            <?php endif; ?>
        </main>
    </div>
    <div class="col-md-3">
        <?php get_sidebar( 'two' ); ?>
    </div>
</div>
<?php get_footer(); ?>
