<?php
/*
Template Name: Main Page
*/

if (function_exists('get_field')) {
    $side_gallery = get_field('acf_slider');
}
?>
<?php get_header(); ?>
<div class="post-content page-content">
    <?php the_post(); the_content(); ?>
</div>
<?php get_footer(); ?>