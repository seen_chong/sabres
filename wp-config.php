<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sabres_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_HOME','http://www.sabressecurity.com/');
define('WP_SITEURL','http://www.sabressecurity.com/');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'a#bW+:ot2YYZ,v]+Nv&C=/eYSxa,;f=:h|v)_1qX^xl||l_=D&jbwK+b&YwcQCm9');
define('SECURE_AUTH_KEY',  'ep%!iej*UO_!_;?&q>@C:{4,{<e8C^)5b%fJD@{1F!Ed5C<~u35NZ^p3q)7/t^E{');
define('LOGGED_IN_KEY',    'J$y?Np-cWz2U,[?UwQs`dpQ*?a$YQ6VtZEI_RB&e@-!?W#r-xs9iM.z5Exh3M4mK');
define('NONCE_KEY',        ';+C=>M&$3$lQLRZa-)S~H;fR~1zP6h}QX#6os]o=2}WP?,5/^P j$dD^+)X>3bS-');
define('AUTH_SALT',        'xk *WW*Sn4^o<IO0]tZB;T7_%4/:p*:D.bcj)IMLoR4NqRx:,jIk Jl=-)QlxQcJ');
define('SECURE_AUTH_SALT', 'U:/-+KN~_l>+RTw*bgeSwwm3>ZF=3+f7t-WP;6GX`.UmObD4:AVOjba8PxH%[EhH');
define('LOGGED_IN_SALT',   '*,.pxVhyVGW9bng {2L }/!{kJ7WHyDFwEGG{Uy.oZT--v@/awb>hEuf$csD0==0');
define('NONCE_SALT',       'vq@s_V#lY|V#i%PszW&pzG<%Ld*-}%|Lj.N|5;fue}|W][9/amtb+bm0a8+=8sT:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
